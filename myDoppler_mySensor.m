clc
clear all
close all

%Laser Doppler Sensor
tic;
%% Input parametrs

c       = 3e8;                  % light speed
n       = 1.33;                 % water refractive index 
T       = 2 * pi;               % period
lambda  = 650e-9;               % laser wavelength
f       = c / lambda;           % laser frequency 
w0      = T * f;                

Ts      = 10;                   % time points     
Fs      = 10*2.3e4;             % sampling frequency
t0      = 0:1/Fs:Ts-1/Fs;       % time axe
f0      = 0:1/Ts:Fs-1/Ts;       % frequency axe

%% Blood/vessel

viscosity       = 3.5e-3;       % 3-4e-3 Pa*sec for 37 degrees
density         = 1055;         % for blood kg/m 1050-1064
diameter        = 5e-6;         % meter 
radius          = diameter / 2; % meter
thickness_wall  = 1e-6;         % meter
length_vessel   = 1e-6;         % meter
% S1               = pi * diameter * diameter / 4;
% S2               = pi * radius * radius;
% S3               = 30e-6;
S               = 50;           % square for full body (m^2)
V_mean          = 0.5e-3;       % average linear blood speed

%% pressure
dias_pressure = 80;             
sist_pressure = 120;            
shear         = sist_pressure - dias_pressure; 
Q_mean        = V_mean * S;     % average volume blood speed
R_mean        = shear / Q_mean; % average hemodynamic resistance
Q_dias        = dias_pressure / R_mean; % min volume blood speed
Q_sist        = sist_pressure / R_mean; % max volume blood speed

%% erythrocyte velocity

Am  = (Q_sist - Q_dias) / 2;
Ah  = Q_dias + Am;

f_u = 100;
w_u = T * f_u;

u_lam   = Ah + Am .* sin(w_u .* t0); % velocity of laminar flow

%radial velocity
 
for ii = 1 : length(t0)
    q = integral(@(x) 2 .* x .* t0(ii), 0, radius);
    Q(ii) = q;                  % velocity of turbulent flow
end

u_tur   = Ah + Am .* Q .* sin(w_u .* t0);

figure()
subplot(211); plot(t0, u_lam)
legend('Velocity of cell - lam')
xlabel('time, sec'); ylabel('length, m')
grid on; hold on
subplot(212); plot(t0, u_tur)
legend('Velocity of cell - tur')
xlabel('time, sec'); ylabel('length, m')
grid on; hold on

%% Doppler shear

O  = 60;        

% laminar flow
                   
f_D_lam = 2 .* u_lam .* sin(O/2) ./ lambda;        % Doppler shear

f1_lam  = f;                                        % for falling beam 
f2_lam  = f + f_D_lam;                              % for reflecting beam 


% turbulent flow

f_D_tur = 2 .* u_tur .* sin(O/2) ./ lambda;

f1_tur  = f;                                          
f2_tur  = f + f_D_tur;                              

figure()
subplot(211); plot(u_lam, f_D_lam)
legend(' Velocity of cell lam (Doppler shift)')
xlabel('velocity  of cell lam, m/s'); ylabel('frequency, Hz')
grid on; hold on
subplot(212); plot(u_tur, f_D_tur)
legend(' Velocity of cell tur(Doppler shift)')
xlabel('velocity  of cell tur, m/s'); ylabel('frequency, Hz')
grid on; hold on

%% intensity

A1  = 1;        % amplitude
A2  = 1;
det = 0.1;      % phase

I1  = A1 * A1;  % intensity
I2  = A2 * A2;

% average intensity
% I3  = I1 + I2 + 2 .* sqrt(I1 .* I2) .* cos(T .* (f1 - f2) .* t0 + det);
% figure()      
% plot(t0, I3)
% title('Intense')
% xlabel('time, s'); ylabel('intense')
% grid on; hold on
% ? Velocity  = 2 * sqrt(I1 * I2) / (I1 + I2); 

%% equations

S1_lam      = A1 .* sin(T .* f1_lam .*t0); % falling 
S2_lam      = A2 .* sin(T .* f2_lam .*t0); % reflecting

S1_tur      = A1 .* sin(T .* f1_tur .*t0); % 
S2_tur      = A2 .* sin(T .* f2_tur .*t0); % 

% interference
S_lam   =  2 .* sin((T .* t0 .* (f1_lam + f2_lam)) ./ 2) .* cos (T .* t0 .* (f1_lam - f2_lam) ./ 2);
S_tur   =  2 .* sin((T .* t0 .* (f1_tur + f2_tur)) ./ 2) .* cos (T .* t0 .* (f1_tur - f2_tur) ./ 2);


%% graphs

% figure() 
% plot(t0, S1, t0, S2)
% title('Signals'); legend('IN','OUT')
% xlabel('time, s'); ylabel('amplitude, V')
% grid on; hold on

% figure() 
% subplot(311); plot(t0, S1)
% legend('IN')
% xlabel('time, s'); ylabel('amplitude, V')
% title('IN and OUT-put, Doppler shift signals')
% grid on; hold on

% subplot(312); plot(t0, S2)
% legend('OUT')
% xlabel('time, s'); ylabel('amplitude, V')
% grid on; hold on

% subplot(313); plot(t0, S_lam)
% legend('Doppler shift')
% xlabel('time, s'); ylabel('amplitude, V')
% grid on; hold on

%% spectrum

sh_sp_lam = fft(S_lam); 
spect_lam = 2 * abs(sh_sp_lam) ./ length(S_lam);

sh_sp_tur = fft(S_tur); 
spect_tur = 2 * abs(sh_sp_tur) ./ length(S_tur);

figure() 
plot(f0, spect_lam)
title('Spectrum-lam')
xlabel('frequency, Hz'); ylabel('amplitude, V')
xlim([0, Fs/2])
grid on; hold on

figure() 
plot(f0, spect_tur)
title('Spectrum-tur')
xlabel('frequency, Hz'); ylabel('amplitude, V')
xlim([0, Fs/2])
grid on; hold on

%% Parametrs

% point_high  = max(spect);             % ��� 
% point       = point_high / sqrt(2);

del_f   = 250e3;                        % bandwidth
% coherence length
L_kog   = c / n / del_f;        
% interference trellis
D_cell  = lambda / (2 * sin(O / 2));
% hardware index
A0      = 2 * sin(O / 2) / lambda;


%% velocity 

% W_lam =
% W_tur =
% 
% figure()
% subplot(211); plot(t0, W_lam)
% title('Velocity')
% xlabel('time, sec'); ylabel('speed, m')
% grid on; hold on
% subplot(212); plot(t0, W_tur)
% title('Velocity')
% xlabel('time, sec'); ylabel('speed, m')
% grid on; hold on
   
% Reynolds number for flow
% Re_lam = radius * W_lam * density / viscosity; % Re ... 200 / >1000 - Not Good(NG)
% Re_tur = radius * W_tur * density / viscosity;
% 
% figure()
% subplot(211); plot(t0, Re_lam)
% title('Reynolds number')
% xlabel('time, sec'); ylabel('number')
% grid on; hold on
% subplot(212); plot(t0, Re_tur)
% title('Reynolds number')
% xlabel('time, sec'); ylabel('number')
% grid on; hold on

time = toc;
hours = time/60/60





